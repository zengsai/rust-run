# Rust Run

*An easy way to compile and run rust source file as script language.*

#install#

1. You need install git, rust and cargo first. Please refer to http://www.rust-lang.org and http://www.creates.io . 
2. clone this repo and complie.

````shell
git clone https://git.oschina.net/zengsai/rust-run.git
cd rust-run
cargo build
````

#usage#

````shell
rust-run [rust-options] [srcfile.rs [options-for-srcfile]]
````

if no srcfile.rs given, rust-run == rustc.

#trick#

using alias for convinent, add the following line to your .bashrc or .profile:

````shell
alias r='rust-run'
````
